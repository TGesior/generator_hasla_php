<?php

function sprawdz_duze_litery($string) {
  // prawda jeżeli małe litry zmienią ciąg
  return strtolower($string) != $string;
}

function sprawdz_male_litery($string) {
  // prawda jeżeli duże litry zmienią ciąg
  return strtoupper($string) != $string;
}

function policz_liczby($string) {
  return preg_match_all('/[0-9]/', $string);
}

function policz_znaki($string) {
  return preg_match_all('/[!@#$%^&*-_+=?]/', $string);
}


function sila_hasla($password) {
  $sila = 0;
  $mozliwe_punkty = 12;
  $length = strlen($password);
  
  if(sprawdz_duze_litery($password)) {
    $sila += 1;
  }
  if(sprawdz_male_litery($password)) {
    $sila += 1;
  }
  
  $sila += min(policz_liczby($password), 2);
  $sila += min(policz_znaki($password), 2);
  
  if($length >= 8) {
    $sila += 2;
    $sila += min(($length -8) * 0.5, 4);
  }
  
  $sila_procent = $sila / (float) $mozliwe_punkty;
  $ocena = floor($sila_procent * 10);
  return $ocena;
}

$haslo = $_POST['ocena'];
$ocena = sila_hasla($haslo);

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Ocena siły hasła</title>
    <style>
    #meter div {
      height: 20px; width: 20px;
      margin: 0 1px 0 0; padding: 0;
      float: left;
      background-color: #DDDDDD;
    }
    #meter div.rating-1, #meter div.rating-2 {
      background-color: green;
    }
    #meter div.rating-3, #meter div.rating-4 {
      background-color: blue;
    }
    #meter div.rating-5, #meter div.rating-6 {
      background-color: yellow;
    }
    #meter div.rating-7, #meter div.rating-8 {
      background-color: orange;
    }
    #meter div.rating-9, #meter div.rating-10 {
      background-color: red;
    }
    </style>
    <link href="styles.css" rel="stylesheet" type="text/css">
  </head>
  <body>

    <p>Ocena Twojego hasła to: <?php echo $ocena; ?>

    <div id="meter">
      <?php
      for($i=0; $i < 10; $i++) {
        echo "<div";
        if($ocena >= $i) {
          echo " class=\"rating-{$ocena}\"";
        }
        echo "></div>";
      }
      ?>
    </div>
    
    <br style="clear: both;" />

    <p><b>Ocen siłę swojego hasła:</b></p>
    <form action="" method="post">
      Wprowadź hasło: <input type="text" name="ocena" value="" /><br />
      <input type="submit" value="Oceń" />
    </form>
<ul id="haslo_wybor">
                <li><a href="index.php">Powrót do menu</a></li> 
            </ul>
  </body>
</html>
