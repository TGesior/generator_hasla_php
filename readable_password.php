<?php
function slowa_slownik($filename=""){
$slownik = $filename;
return file($slownik, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}


function wybierz_losowa($array){
    $i = mt_rand(0,count($array)-1);
    return $array[$i];
    
}
function wybierz_losowy_znak(){
    $znaki = "!@#$%^&*()";
    $i =  mt_rand(0,strlen($znaki)-1);
    return $znaki[$i];
}
function wybierz_losowa_liczbe($cyfry=1){
    $min = pow(10,($cyfry-1));
    $max = pow(10,$cyfry) -1;
    return strval(mt_rand($min,$max));
}

function filtruj_dlugosc($array,$lenght){
    $wybierz_slowa=array();
    foreach($array as $word){
        if(strlen($word) == $lenght){
            $wybierz_slowa[] = $word;
        }
    }
    return $wybierz_slowa;
}


$podstawowe_slowa = slowa_slownik('friendly_words.txt');
$slowa_firmowe = slowa_slownik('brand_words.txt');

$words = array_merge($slowa_firmowe, $podstawowe_slowa);

$dlugosc = 12;
$ilosc_slow = 2;
$ilosc_liczb = 2;
$ilosc_znakow = 1;
$srednia = ($dlugosc - $ilosc_liczb - $ilosc_znakow) / $ilosc_slow;

$haslo = '';
$next_wlenght = mt_rand($srednia -1,$srednia +1);
$wybierz_slowa = filtruj_dlugosc($words,$next_wlenght);
$haslo .= wybierz_losowa($wybierz_slowa);
$haslo .= wybierz_losowy_znak();
$haslo .= wybierz_losowa_liczbe($ilosc_liczb);
$next_wlenght = $dlugosc - strlen($haslo);
$wybierz_slowa = filtruj_dlugosc($words,$next_wlenght);
$haslo .= wybierz_losowa($wybierz_slowa);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Przyjazne hasło</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <p><b>GENERATOR PRZYJAZNYCH HASEŁ</b></p>
    <p>Twoje przyjazne hasło to: <?php echo'<span class="red">' . $haslo . "</span>"; ?></p>
    <p>Długość Twojego hasła to: <?php echo'<span class="red">' . strlen($haslo) . "</span>"; ?></p>
    
   
    <ul id="haslo_wybor">
                <li><a href="readable_password.php">Generuj nowe przyjazne hasło</a></li>
                <li><a href="index.php">Powrót do menu</a></li>
                
    </ul>
  </body>
</html>


