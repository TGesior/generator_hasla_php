<?php
function losowa_liczba($zakres){
   
    $losowa = mt_rand(0,strlen($zakres)-1);
    return $zakres[$losowa];
    
}

function losowe_liczby($liczba,$zakres){
    $losowe_liczby = '';
    for($i=0;$i<$liczba;$i++){
        $losowe_liczby .= losowa_liczba($zakres);
    }
    return $losowe_liczby;
}

function generuj_haslo($opcje){
$male = join(range('a','z'));
$duze = join(range('A','Z'));
$liczby = join(range('0','9'));
$znaki = '!@#$%^&*()';

$uzyte_male = isset($opcje['male']) ? $opcje['male'] : '0';
$uzyte_duze = isset($opcje['duze']) ? $opcje['duze'] : '0';
$uzyte_liczby = isset($opcje['liczby']) ? $opcje['liczby'] : '0';
$uzyte_znaki = isset($opcje['znaki']) ? $opcje['znaki'] : '0';
$ciag = '';
if($uzyte_male === '1'){
    $ciag .= $male;
}
if($uzyte_duze === '1'){
    $ciag .= $duze;
}
if($uzyte_liczby === '1'){
    $ciag .= $liczby;
}
if($uzyte_znaki === '1'){
    $ciag .= $znaki;
}    
$liczba = isset($opcje['dlugosc']) ? $opcje['dlugosc'] : 8;
return losowe_liczby($liczba,$ciag);
}
$opcje = array(
  'dlugosc' => $_GET['dlugosc'],
    'male' => $_GET['male'],
    'duze' => $_GET['duze'],
    'liczby' => $_GET['liczby'],
    'znaki' => $_GET['znaki']
);
$haslo = generuj_haslo($opcje);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Generator hasła</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
  </head>
  <body>

    <p>Wygenerowane hasło: <?php echo'<span class="red">' . $haslo . "</span>"; ?></p>
    
    <p><b>GENERATOR HASEŁ</b></p>
    <form action="" method="get">
      Długość: <input type="text" name="dlugosc" value="<?php if(isset($_GET['dlugosc'])) { echo $_GET['dlugosc']; } ?>" /><br />
      <input type="checkbox" name="male" value="1" <?php if($_GET['male'] == 1) { echo 'checked'; } ?> /> Małe<br />
      <input type="checkbox" name="duze" value="1" <?php if($_GET['duze'] == 1) { echo 'checked'; } ?> /> Duże<br />
      <input type="checkbox" name="liczby" value="1" <?php if($_GET['liczby'] == 1) { echo 'checked'; } ?> /> Liczby<br />
      <input type="checkbox" name="znaki" value="1" <?php if($_GET['znaki'] == 1) { echo 'checked'; } ?> /> Znaki<br />
      <input type="submit" value="Generuj hasło" />
    </form>
    <ul id="haslo_wybor">
                <li><a href="password_generator.php">Generuj nowe hasło</a></li>
                <li><a href="index.php">Powrót do menu</a></li>
                
            </ul>
  </body>
</html>
